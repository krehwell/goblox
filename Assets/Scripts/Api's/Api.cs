﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Api : MonoBehaviour
{
    public static string URI = "http://api.tenenet.net/";
    public static string token = "1d3e5051f223b74fd6bf7d993f087bad";
    public static string leaderboardid = "gobloxleaderboard";
    public static string matricid = "gobloxmetric";

    public delegate void leaderboardCb(LeaderboardJSON data);
    public delegate void scoreCb(ScoreJson score);

    public static Api Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public IEnumerator UpdateScore(int score)
    {
        string username = PlayerPrefs.GetString("username");

        yield return null;

        StartCoroutine(GetScore(username, (scoredata) =>
        {
            int userHighestScore = int.Parse(scoredata.message.score[0].value);
            if (score > userHighestScore)
            {
                int scoreToUpdate = score - userHighestScore;
                UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + username + "&id=" + matricid + "&operator=add&value=" + scoreToUpdate);
                www.SendWebRequest();
            }
        }));
    }

    [ContextMenu("Reset Player Score")]
    public void UpdatePlayerScoreToZero()
    {
        string username = "yuza"; // change this name
        StartCoroutine(GetScore(username, (scoredata) =>
        {
            int playerScore = int.Parse(scoredata.message.score[0].value);
            UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + username + "&id=" + matricid + "&operator=add&value=" + -playerScore);
            www.SendWebRequest();
        }));
    }

    public IEnumerator RegisterPlayer(string username)
    {
        UnityWebRequest www = UnityWebRequest.Get(Api.URI + "/createPlayer" + "?token=" + Api.token + "&alias=" + username + "&id= &fname= &lname= ");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            // status.text = "There was an error with the your connection";
        }

        Response res = new Response();
        res = JsonUtility.FromJson<Response>(www.downloadHandler.text);

        if (res.message == "player_exists")
        { }
        else
        { }
    }

    public IEnumerator GetLeaderboard(leaderboardCb data)
    {
        Debug.Log("Start to leaderboard");
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getLeaderboard" + "?token=" + token + "&id=" + leaderboardid);

        yield return www.SendWebRequest();

        LeaderboardJSON res = new LeaderboardJSON();
        res = JsonUtility.FromJson<LeaderboardJSON>(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("Error occur");
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("I will response");
            data(res);
        }
    }

    public IEnumerator GetScore(string _username, scoreCb data)
    {
        UnityWebRequest www = UnityWebRequest.Get(Api.URI + "/getPlayer" + "?token=" + Api.token + "&alias=" + _username);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }

        ScoreJson res = new ScoreJson();
        res = JsonUtility.FromJson<ScoreJson>(www.downloadHandler.text);

        data(res);
    }

}
