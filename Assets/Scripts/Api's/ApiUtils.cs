﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LeaderboardData
{
    public string alias;
    public string rank;
}

[System.Serializable]
public class LeaderboardDataRaw
{
    public LeaderboardData[] data;
}

[System.Serializable]
public class LeaderboardJSON
{
    public LeaderboardDataRaw message;
    public string status;
}

[System.Serializable]
public class Response
{
    public string message;
    public string status;
}

[System.Serializable]
public class ScoreData
{
    public string value;
}

[System.Serializable]
public class ScoreDataRaw
{
    public List<ScoreData> score;
}

[System.Serializable]
public class ScoreJson
{
    public ScoreDataRaw message;
    public string status;
}

public class PlayerScore
{
    public string username;
    public int score;
}

public class ApiUtils : MonoBehaviour
{ }
