﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using TMPro;

public class LeaderboardManager : MonoBehaviour
{
    [Header("Leaderboard Props")]
    public Transform leaderboardGrid;
    public GameObject board;
    public Transform gridview;
    public List<PlayerScore> playerScoreList = new List<PlayerScore>();

    public void RemoveAllBoards()
    {
        foreach(Transform gridviewChild in gridview)
        {
            Destroy(gridviewChild.gameObject);
        }
        playerScoreList.Clear();
    }

    public void StartFetchLeaderBoard()
    {
        StartCoroutine(FetchLeaderBoard());
    }

    public IEnumerator FetchLeaderBoard()
    {
        bool isNotFinishFetching = true;

        StartCoroutine(Api.Instance.GetLeaderboard(data =>
        {
            int i = 0;
            Debug.Log("about to count");
            Debug.Log(data.message.data);

            foreach (var item in data.message.data)
            {
                string username = item.alias;
                string score = "";

                StartCoroutine(Api.Instance.GetScore(username, (scoreObj) =>
                {
                    PlayerScore newPlayer = new PlayerScore();

                    score = scoreObj.message.score[0].value;

                    newPlayer.username = username;
                    newPlayer.score = int.Parse(score);

                    playerScoreList.Add(newPlayer);

                    i++;
                    if (i == data.message.data.Length)
                    {
                        isNotFinishFetching = false;
                    }

                    Debug.Log(i);
                }));
            }
        }));

        yield return new WaitWhile(() => isNotFinishFetching == true);

        RenderScore();
    }

    void RenderScore()
    {
        playerScoreList.Sort((p1, p2) => p2.score.CompareTo(p1.score));

        foreach (var player in playerScoreList)
        {
            Debug.Log(player.username);
            Debug.Log(player.score);

            GameObject _boardName = Instantiate(board, Vector3.one, Quaternion.identity, leaderboardGrid);
            _boardName.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(player.username);

            GameObject _boardRank = Instantiate(board, Vector3.one, Quaternion.identity, leaderboardGrid);
            _boardRank.transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(player.score.ToString());
        }
    }
}

