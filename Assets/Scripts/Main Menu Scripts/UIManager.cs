using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject loginMenu;
    public GameObject playMenu;
    public GameObject leaderBoardMenu;
    public InputField usernameIF;

    public LeaderboardManager leaderboardManager;

    // Start is called before the first frame update
    void Start()
    {
        loginMenu.SetActive(true);
        playMenu.SetActive(false);
        leaderBoardMenu.SetActive(false);
    }

    public void PlayBtn()
    {
        SceneManager.LoadScene("Game");
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }

    public void loginBtn()
    {
        if (usernameIF.text != "")
        {
            StartCoroutine(Api.Instance.RegisterPlayer(usernameIF.text));
            loginMenu.SetActive(false);
            playMenu.SetActive(true);
            leaderBoardMenu.SetActive(false);
            PlayerPrefs.SetString("username", usernameIF.text);
            SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
        }
    }

    public void logOutBtn()
    {
        loginMenu.SetActive(true);
        playMenu.SetActive(false);
        leaderBoardMenu.SetActive(false);
        PlayerPrefs.DeleteKey("username");
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }

    public void leaderBoardBtn()
    {
        loginMenu.SetActive(false);
        playMenu.SetActive(false);
        leaderBoardMenu.SetActive(true);
        leaderboardManager.StartFetchLeaderBoard();
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }

    public void backBtn()
    {
        loginMenu.SetActive(false);
        playMenu.SetActive(true);
        leaderBoardMenu.SetActive(false);
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }
}
