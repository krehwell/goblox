﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackManager : MonoBehaviour
{
    [SerializeField] private List<Track> trackList = new List<Track>();
    public List<Track> getPlayerList => trackList;

    void Awake()
    {
        GetAllTracks();
    }

    // TESTING GenerateObstacle();
    void Start()
    { }

    private void GetAllTracks()
    {
        Track[] tracks = GetComponentsInChildren<Track>();
        foreach (Track track in tracks)
        {
            trackList.Add(track);
        }
    }

    public void GenerateObstacle(int numOfObs = 1)
    {
        int numOfObsSpawned = 0;
        foreach (Track track in trackList)
        {
            bool shouldSpawn = (Random.Range(0, 100) < 20);
            if (shouldSpawn && numOfObsSpawned < numOfObs)
            {
                track.PutObstacle();
                numOfObsSpawned++;
            }
        }
    }
}
