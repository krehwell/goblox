﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallManager : MonoBehaviour
{
    [SerializeField] private List<Wall> wallList = new List<Wall>();
    public List<Wall> getWallList => wallList;

    void Awake()
    {
        GetAllWalls();
    }

    private void GetAllWalls()
    {
        Wall[] walls = GetComponentsInChildren<Wall>();
        foreach (Wall wall in walls)
        {
            wallList.Add(wall);
        }
    }

    public IEnumerator CheckIsWallAtPlayerPos()
    {
        yield return new WaitUntil(() => wallList[0].iAmAtPlayerPos == true);
        yield return new WaitForSeconds(0.5f);
        StopAllCoroutines();
        GameManager.Instance.SpawnNewLevel();
    }

    public void ResetAllWalls()
    {
        foreach (Wall wall in wallList)
        {
            wall.ResetMyPos();
            wall.DestroyAllChild();
            wall.DisableMyMesh(false);
            wall.SetPlayerMustHitMe(false);
            wall.StartMovingWall();
        }
        StartCoroutine(CheckIsWallAtPlayerPos());
    }
}
