﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour
{
    public Obstacle obstacle;
    public Transform myOwnTranform;

    void Awake()
    {
        myOwnTranform = transform;
    }

    public void PutObstacle()
    {
        obstacle.Spawn(myOwnTranform);
    }
}
