﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Wall : MonoBehaviour
{
    [Header("Wall State")]
    [SerializeField] private Vector3 initialPos;
    [SerializeField] private bool playerMustHitMe = false;
    public bool iAmAtPlayerPos = false;
    public List<GameObject> prefabCube = new List<GameObject>();

    void Awake()
    {
        // TESTING
        initialPos = transform.position;
        initialPos.z = 110f;
    }

    [ContextMenu("Spawn Prefab")]
    public void SpawnPrefab()
    {
        int c = Random.Range(0, prefabCube.Count);
        Instantiate(prefabCube[c], transform.position, transform.rotation * Quaternion.Euler (0f, 180f, 0f), transform);
    }

    public void StartMovingWall()
    {
        StartCoroutine(MoveWall());
    }

    public IEnumerator MoveWall()
    {
        int WallSpeed = GameManager.Instance.getWallSpeed;
        var wallIsMoving = transform.DOMoveZ(GameManager.Instance.getPivotPlayerPos.z, WallSpeed).SetEase(Ease.Linear);

        yield return wallIsMoving.WaitForCompletion();
        // yield return new WaitForSeconds(1f);
        iAmAtPlayerPos = true;
    }

    public void SetPlayerMustHitMe(bool b)
    {
        playerMustHitMe = b;
    }

    public bool GetPlayerMustHitMe()
    {
        return playerMustHitMe;
    }

    public void DisableMyMesh(bool b = false)
    {
        // DestroyAllChild();
        // GetComponent<MeshRenderer>().enabled = !b;
        if (b == false)
        {
            SpawnPrefab();
        }
        if (b == true)
        {
            foreach (Transform child in transform) {
                GameObject.Destroy(child.gameObject);
            }
            SetPlayerMustHitMe(true);
        }
    }

    public void DestroyAllChild()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        SetPlayerMustHitMe(false);
    }

    public void ResetMyPos()
    {
        iAmAtPlayerPos = false;
        transform.position = initialPos;
    }
}
