﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Obstacle : MonoBehaviour
{
    public GameObject prefab;

    public void Spawn(Transform trackTile)
    {
        Vector3 pos = trackTile.position;
        pos.y = 0.3f;
        GameObject obs = Instantiate(prefab, pos, Quaternion.identity);
        obs.GetComponent<Obstacle>().StartMovingObs();
    }

    public void StartMovingObs()
    {
        StartCoroutine(MoveObs());
    }

    public IEnumerator MoveObs()
    {
        int obsSpeed = GameManager.Instance.getWallSpeed - 3;
        float obsEndPosZ = GameManager.Instance.getPivotPlayerPos.z - 5;
        var obsIsMoving = transform.DOMoveZ(obsEndPosZ, obsSpeed).SetEase(Ease.Linear);
        transform.GetChild(0).transform.DOLocalRotate(new Vector3(1500, 1500, 0), 10f, RotateMode.FastBeyond360);
        yield return obsIsMoving.WaitForCompletion();
        Destroy(gameObject);
    }
}
