﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [Header("Game Configuration")]
    [SerializeField] private int wallSpeed = 10;
    public int getWallSpeed => wallSpeed;
    [SerializeField] private int gameComplexity;  // level. how many cube should be removed on wall
    [SerializeField] private int distractionProbability = 20;

    [Header("Gameplay Vars")]
    [SerializeField] private int numOfPlayerDeleted;
    [SerializeField] private int maxPlayerToDelete;
    public bool isPlaying = true;
    public int score;

    [Header("UI Purposes")]
    public Text scoreText;
    public Transform gameOverPanel;
    public GameObject pauseBtn;
    public GameObject resumeBtn;
    public GameObject logOutBtn;
    public TextMeshProUGUI gameOverScoreTxt;

    [Header("Pivot Initialization")]
    [SerializeField] private Vector3 pPlayerPos;
    [SerializeField] private Vector3 pWallPos;

    public Vector3 getPivotPlayerPos => pPlayerPos;
    public Vector3 getPivotWallPos => pWallPos;

    [Header("Game Entities Manager")]
    public PlayerManager playerManager;
    public List<int> indexOfPlayer = new List<int>();
    public WallManager wallManager;
    public TrackManager trackManager;

    // singleton
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
        Time.timeScale = 1;
        StartCoroutine(StartCalculateScore());
        GenerateNewLevel(gameComplexity, distractionProbability);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public IEnumerator StartCalculateScore()
    {
        while (isPlaying)
        {
            yield return new WaitForSeconds(0.5f);
            score++;
            scoreText.text = score.ToString();
        }
    }

    bool acceptActionCalled = true;
    public void GameOverAction()
    {
        if (acceptActionCalled == false)
        {
            return;
        }
        acceptActionCalled = false;
        StartCoroutine(Api.Instance.UpdateScore(score));
        gameOverPanel.gameObject.SetActive(true);
        gameOverScoreTxt.GetComponent<TextMeshProUGUI>().text = scoreText.text;
        isPlaying = false;
        Time.timeScale = 0;
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.gameOver);
    }

    /****** LEVEL CONFIGURATION ******/
    public void GenerateLevelParams()
    {
        if (score < 50)
        { }
        else if (score < 100)
        { SetLevelParams(10, 4, 20); }
        else if (score < 150)
        { SetLevelParams(7, 3, 20); }
        else if (score < 200)
        { SetLevelParams(8, 4, 50); }
        else if (score < 250)
        { SetLevelParams(10, 5, 20); }
        else if (score < 320)
        { SetLevelParams(10, 5, 40); }
        else if (score < 350)
        { SetLevelParams(10, 6, 50); }
        else if (score < 400)
        { SetLevelParams(12, 7, 30); }
    }

    public void SetLevelParams(int ws, int gc, int dp)
    {
        wallSpeed = ws;
        gameComplexity = gc;
        distractionProbability = dp;
    }

    public void GenerateNewLevel(int complexity, int dProb)
    {
        // CALL FUNCTION TO CALCULATE WHAT SHOULD BE THE LEVEL ON FUNCTION PARAMS BELOW
        GenerateLevelParams();

        wallManager.ResetAllWalls();
        playerManager.ResetAllPlayers();

        // Should Obstacle Spawn?
        if (Random.Range(0, 100) < 55)
        {
            trackManager.GenerateObstacle();
        }

        // destructuring
        var _walls = wallManager.getWallList;
        var _players = playerManager.getPlayerList;

        int numOfEmptyCubeOnWall = 0;

        for (int i = 0; i < wallManager.getWallList.Count; i++)
        {
            bool shouldBeEmptyCube = (Random.Range(0, 100) < dProb);
            if (shouldBeEmptyCube)
            {
                if (numOfEmptyCubeOnWall < complexity)
                {
                    _walls[i].DisableMyMesh(true);
                    _players[i].DisableMyMesh(false);
                    indexOfPlayer.Add(i);

                    numOfEmptyCubeOnWall++;
                }
            }
        }

        int numOfCubeHasBeenSetToBeDistract = 0;
        int numOfCubeToDistractPlayer = Random.Range(complexity, complexity + 2);
        // maxPlayerToDelete = numOfCubeToDistractPlayer;

        for (int i = 0; i < playerManager.getPlayerList.Count; i++)
        {
            if (_players[i].isMyMeshNotEmpty) continue;

            bool shouldBeDistract = (Random.Range(0, 100) < dProb);
            if (numOfCubeHasBeenSetToBeDistract < numOfCubeToDistractPlayer)
            {
                _players[i].DisableMyMesh(false);
                indexOfPlayer.Add(i);

                numOfCubeHasBeenSetToBeDistract++;
                maxPlayerToDelete++;
            }
        }
    }

    public void SpawnNewLevel(int gC = 3, int p = 20)
    {
        // SceneManager.LoadScene("Game");
        ResetGameManager();
        gC = gameComplexity;
        p = distractionProbability;
        GenerateNewLevel(gC, p);
    }

    public void ResetGameManager()
    {
        numOfPlayerDeleted = 0;
        maxPlayerToDelete = 0;
        indexOfPlayer.Clear();
    }

    /****** CHECKERS ******/
    public bool AllowPlayerToBeDeleted()
    {
        return numOfPlayerDeleted < maxPlayerToDelete;
    }

    public void IncrementNumOfPlayerDeleted()
    {
        numOfPlayerDeleted++;
    }

    /****** MENU UI ******/
    public void ResetButton()
    {
        var _players = playerManager.getPlayerList;
        foreach (int i in indexOfPlayer)
        {
            _players[i].DisableMyMesh(false);
        }
        numOfPlayerDeleted = 0;
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }

    public void MoveRightButton()
    {
        playerManager.MoveRight();
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.moveButton);
    }

    public void MoveLeftButton()
    {
        playerManager.MoveLeft();
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.moveButton);
    }

    public void GoBackToMenu()
    {
        PlayerPrefs.DeleteKey("username");
        SceneManager.LoadScene("UIMainMenuScene");
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }

    public void PauseGame()
    {
        isPlaying = false;
        Time.timeScale = 0;
        pauseBtn.SetActive(false);
        resumeBtn.SetActive(true);
        logOutBtn.SetActive(true);
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }

    public void ResumeGame()
    {
        isPlaying = true;
        Time.timeScale = 1;
        pauseBtn.SetActive(true);
        resumeBtn.SetActive(false);
        logOutBtn.SetActive(false);
        SfxManager.sfxInstance.Audio.PlayOneShot(SfxManager.sfxInstance.Click);
    }
}
