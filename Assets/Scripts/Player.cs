﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    [Header("Player State")]
    [SerializeField] private Vector3 initialPos;
    public bool isMyMeshNotEmpty = true;
    public List<GameObject> prefabCube = new List<GameObject>();

    void Awake()
    {
        initialPos = transform.position;
    }

    void OnMouseDown()
    {
        // Debug.Log(transform.name);
        if (GameManager.Instance.AllowPlayerToBeDeleted())
        {
            DisableMyMesh(true);
            GameManager.Instance.IncrementNumOfPlayerDeleted();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        CheckIsHitingWall(other.transform);
    }

    [ContextMenu("Spawn Prefab")]
    void SpawnPrefab()
    {
        int c = Random.Range(0, prefabCube.Count);
        Instantiate(prefabCube[c], transform.position, transform.rotation * Quaternion.Euler (0f, 180f, 0f), transform);
    }

    public void StartMovingPlayer()
    {
        StartCoroutine(MovingPlayer());
        transform.position = initialPos;
    }

    public IEnumerator MovingPlayer()
    {
        yield return null;
        transform.position = new Vector3(Random.Range(-100, 200), Random.Range(-100, 200), Random.Range(-200, -100));
        var a = transform.DOMove(initialPos, 1.0f).SetEase(Ease.InOutCubic);
        // yield return a.WaitForCompletion();
        transform.DOShakeRotation(1.0f, 75, 7, 200).SetEase(Ease.InOutCubic);
    }

    public void CheckIsHitingWall(Transform obs)
    {
        if (obs.tag == "wall")
        {
            bool success = obs.GetComponent<Wall>().GetPlayerMustHitMe();
            Debug.LogFormat("player {0} - hit wall - {1} - {2}", transform.name, obs.name, success);
            if (!success)
            {
                GameManager.Instance.GameOverAction();
            }
        }

        if (obs.tag == "obstacle")
        {
            Debug.LogFormat("player {0} - hit obstacle - {1} - {2}", transform.name, obs.name, "false");
            GameManager.Instance.GameOverAction();
        }
    }

    public void DisableMyMesh(bool b = false)
    {
        // GetComponent<MeshRenderer>().enabled = !b;
        DestroyAllChild(); // somehow this must be here to fix the bug. fuck it then!!
        if (b == false)
        {
            SpawnPrefab();
        }
        else if (b == true)
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        GetComponent<BoxCollider>().enabled = !b;
        isMyMeshNotEmpty = !b;
    }

    public void DestroyAllChild()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        GetComponent<BoxCollider>().enabled = false;
        isMyMeshNotEmpty = true;
    }

    public void MoveLeft(TweenCallback cb)
    {
        float curXPos = transform.position.x;
        transform.DOMoveX(curXPos - 1, 0.35f).OnComplete(cb);
    }

    public void MoveRight(TweenCallback cb)
    {
        float curXPos = transform.position.x;
        transform.DOMoveX(curXPos + 1, 0.35f).OnComplete(cb);
    }
}
