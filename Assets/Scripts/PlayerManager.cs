﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private List<Player> playerList = new List<Player>();
    public List<Player> getPlayerList => playerList;

    [Header("Player Manager Stats")]
    [SerializeField] private bool isMoving = false;
    [SerializeField] private int moveValue = 0;
    private int minMoveValue = -2, maxMoveValue = 2;

    void Awake()
    {
        GetAllPlayers();
    }

    void Update()
    {
        if (!isMoving)
        {
            MovementListener();
        }
    }

    // DEBUGGER
    public void MovementListener()
    {
        if (Input.GetKeyDown("a") && (moveValue > minMoveValue))
        {
            isMoving = true;
            foreach (Player player in playerList)
            {
                player.MoveLeft(() => isMoving = false);
            }
            moveValue--;
        }

        else if (Input.GetKeyDown("d") && (moveValue < maxMoveValue))
        {
            isMoving = true;
            foreach (Player player in playerList)
            {
                player.MoveRight(() => isMoving = false);
            }
            moveValue++;
        }
    }

    public void MoveLeft()
    {
        if ((moveValue > minMoveValue) && !isMoving)
        {
            isMoving = true;
            foreach (Player player in playerList)
            {
                player.MoveLeft(() => isMoving = false);
            }
            moveValue--;
        }
    }

    public void MoveRight()
    {
        if ((moveValue < maxMoveValue) && !isMoving)
        {
            isMoving = true;
            foreach (Player player in playerList)
            {
                player.MoveRight(() => isMoving = false);
            }
            moveValue++;
        }
    }

    private void GetAllPlayers()
    {
        Player[] players = GetComponentsInChildren<Player>();
        foreach (Player player in players)
        {
            playerList.Add(player);
        }
    }

    public void ResetAllPlayers()
    {
        moveValue = 0;
        foreach (Player player in playerList)
        {
            // player.DestroyAllChild();
            player.DisableMyMesh(true);
            player.StartMovingPlayer();
        }
    }
}
