# Goblox
Goblox is a game, box? but blox? whatever, you define it yourself.

# Gameplay:
[Link (Youtube)](https://youtu.be/jc9qDfUmU1Q?t=20)

## How to Run
- Clone repo: `git clone "https://gitlab.com/krehwell/goblox.git"` _(it's better to use ssh but whatever)_
- Add Existing Project in Unity -> Target This Repo.

## How to contribute
- `cd goblox`
- `git checkout -b branchname`
- start developing, after finished...
- `git add .`
- `git commit -m "featurename added"`
- `git push origin branchname`

## Caveat
- Make sure to always keep upated with master:
  - `git checkout master` -> `git pull origin master` -> `git checkout branchname` -> `git merge master`
- Make sure remote is this repo:
  - check remote: `git remote -v`, incase empty...
  - add remote: `git remote add origin "https://gitlab.com/krehwell/goblox.git"`

## Dependencies
- Unity 2019.4.x (lts)
- DoTween (Asset Store -> DOTween (HOTween v2) -> Install -> import)
- Aspect Ratio used is Iphone8 _750 x 1334_ Potrait (Inside this project in unity -> go to game window -> add new) [refer here](https://i.imgur.com/bxXSCz8.png)

## Project Structure
```
- Assets
  |- Images      # UI image things
  |- Objects
    |- Material  # material for 3d object
    |- ...       # just bundle all 3d objects here
  |- Scene       # main menu, game main scene, & probably leaderboard scene
  |- Scripts
    |- Api's     # any script consists networking
    |- Main Menu Script  # script for main menu handler
    |- ...       # the rest just bundle all script in here for ease of access
  |- ...
```
